Version française:

# Diagrammes PlantUML

Ce dépôt contient une collection de diagrammes créés avec PlantUML dans Visual Studio Code.
Plantuml permet de faire du diagrams as code.
## Configuration

Pour visualiser et modifier les diagrammes, suivez les étapes suivantes :

1. Clonez le dépôt.
2. Installez l'extension PlantUML dans Visual Studio Code.
3. Ouvrez le dossier dans Visual Studio Code.
4. Pour prévisualiser un diagramme, ouvrez le fichier .puml correspondant et cliquez sur l'icône "Aperçu" dans la barre d'outils PlantUML.

## Contenu

Les diagrammes inclus dans ce dépôt sont organisés par catégories :

* Diagrammes de cas d'utilisation
* Diagrammes de séquence
* Diagrammes de classes
* etc.

N'hésitez pas à contribuer en ajoutant vos propres diagrammes ou en améliorant ceux existants.

## Contribution

Pour contribuer à ce dépôt, créez une branche pour vos modifications, puis ouvrez une pull request.

---

English version:

# PlantUML Diagrams

This repository contains a collection of diagrams created with PlantUML in Visual Studio Code.
Plantuml lets you create diagrams as code.
## Setup

To view and edit the diagrams, follow these steps:

1. Clone the repository.
2. Install the PlantUML extension in Visual Studio Code.
3. Open the folder in Visual Studio Code.
4. To preview a diagram, open the corresponding .puml file and click on the "Preview" icon in the PlantUML toolbar.

## Contents

The diagrams included in this repository are organized by categories:

* Use case diagrams
* Sequence diagrams
* Class diagrams
* etc.

Feel free to contribute by adding your own diagrams or improving existing ones.

## Contribution

To contribute to this repository, create a branch for your changes and open a pull request.